1. Membuat database

create database myshop;

2. Membuat tabel di dalam database

users

CREATE TABLE users ( 
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255), 
	email varchar(255), 
	password varchar(255), 
	PRIMARY KEY (id) 
)

items

CREATE TABLE items ( 
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255), 
	description varchar(255), 
	price int, 
	stock int, 
	category_id int, 
	PRIMARY KEY (id), 
	FOREIGN KEY (category_id) REFERENCES categories(id) 
)

categories

CREATE TABLE categories ( 
	id int NOT NULL AUTO_INCREMENT, 
	name varchar(255), 
	PRIMARY KEY (id)
)

3. Memasukkan data dalam tabel

users

INSERT INTO users (name, email, password) 
VALUES 
	('John Doe', 'john@doe.com', 'john123'), 
	('Jane Doe', 'jane@doe.com', 'jenita123')

items
INSERT INTO categories (name) 
VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded')

categories
INSERT INTO items ( name, description, price, stock, category_id ) 
VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), 
	('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), 
	('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1)


4. Mengambil data dalam tabel

a. Mengambil data users

SELECT name, email FROM `users` WHERE 1

b. mengambil data items

- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT * FROM `items` WHERE price >= 1000000

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) 
	dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM `items` WHERE name LIKE '%sang%'

c. mengambil data items join dengan kategori

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name 
FROM `items` 
INNER JOIN `categories` 
ON items.category_id = categories.id

5. Mengubah data dari database

UPDATE items SET price = 2500000 WHERE name LIKE '%sumsang%'